package de.westwing.campaignbrowser

import de.westwing.campaignbrowser.data.ApiInterface
import de.westwing.campaignbrowser.data.CampaignRepositoryImpl
import de.westwing.campaignbrowser.data.entity.CampaignDto
import de.westwing.campaignbrowser.data.entity.CampaignsMetadata
import de.westwing.campaignbrowser.data.entity.CampaignsResponse
import de.westwing.campaignbrowser.data.entity.ImageDto
import de.westwing.campaignbrowser.domain.CampaignRepository
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class CampaignRepositoryTest {

    @Mock
    private lateinit var apiInterface: ApiInterface

    private lateinit var repository: CampaignRepository

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        repository = CampaignRepositoryImpl(apiInterface)
    }

    @Test
    fun `CampaignRepository returns non-empty list of campaigns`() {
        `when`(apiInterface.getCampaigns()).thenReturn(Single.just(SHORT_RESPONSE))
        val observer = repository.getCampaigns().test()

        observer.assertNoErrors()
        observer.assertComplete()
        observer.assertValue {
            it.isNotEmpty()
        }
    }

    companion object {
        private val IMAGE: ImageDto = ImageDto("url")
        private val CAMPAIGN = CampaignDto("name", "description", IMAGE)
        private val SHORT_RESPONSE = CampaignsResponse(
            metadata = CampaignsMetadata(
                data = listOf(
                    CAMPAIGN
                )
            )
        )
    }
}