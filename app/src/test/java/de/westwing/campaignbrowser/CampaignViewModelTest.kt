package de.westwing.campaignbrowser

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import de.westwing.campaignbrowser.domain.Campaign
import de.westwing.campaignbrowser.domain.CampaignRepository
import de.westwing.campaignbrowser.domain.GetCampaignListUseCase
import de.westwing.campaignbrowser.presentation.CampaignListResult
import de.westwing.campaignbrowser.presentation.CampaignViewModel
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit

class CampaignViewModelTest {

    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Mock
    private lateinit var repository: CampaignRepository
    private val useCase: GetCampaignListUseCase by lazy {
        GetCampaignListUseCase(repository)
    }

    @Mock
    private lateinit var observer: Observer<CampaignListResult>

    private val viewModel: CampaignViewModel by lazy {
        CampaignViewModel(useCase)
    }

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        viewModel.campaignsData.observeForever(observer)
    }

    @Test
    fun `Load data - item list`() {
        `when`(repository.getCampaigns()).thenReturn(Single.just(listOf(SAMPLE_CAMPAIGN)))

        viewModel.loadData()

        verify(observer).onChanged(CampaignListResult.Success(listOf(SAMPLE_CAMPAIGN)))
    }

    @Test
    fun `Load data - error`() {
        `when`(repository.getCampaigns()).thenReturn(Single.fromCallable { throw Exception() })

        viewModel.loadData()

        verify(observer).onChanged(CampaignListResult.Error)
    }

    companion object {
        private val SAMPLE_CAMPAIGN = Campaign("name", "description", "url")
    }
}