package de.westwing.campaignbrowser.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Campaign(val name: String, val description: String, val imageUrl: String) : Parcelable