package de.westwing.campaignbrowser.presentation.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.westwing.campaignbrowser.R
import de.westwing.campaignbrowser.databinding.ItemCampaignBinding
import de.westwing.campaignbrowser.domain.Campaign

class CampaignListAdapter(
    private val itemClick: (Campaign) -> Unit
) : ListAdapter<Campaign, CampaignViewHolder>(campaignDiff) {

    companion object {
        val campaignDiff = object : DiffUtil.ItemCallback<Campaign>() {
            override fun areItemsTheSame(oldItem: Campaign, newItem: Campaign) = oldItem == newItem
            override fun areContentsTheSame(oldItem: Campaign, newItem: Campaign) =
                oldItem.name == newItem.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CampaignViewHolder {
        return CampaignViewHolder(
            ItemCampaignBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: CampaignViewHolder, position: Int) {
        val campaign = getItem(position)
        holder.binding.campaignName.text = campaign.name
        holder.binding.campaignDescription.text = campaign.description

        Glide
            .with(holder.itemView.context)
            .load(campaign.imageUrl)
            .centerCrop()
            .placeholder(R.drawable.image_placeholder)
            .into(holder.binding.campaignImage)

        holder.itemView.setOnClickListener { itemClick(campaign) }
    }
}

class CampaignViewHolder(internal val binding: ItemCampaignBinding) :
    RecyclerView.ViewHolder(binding.root)