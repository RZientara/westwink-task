package de.westwing.campaignbrowser.presentation.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import dagger.android.AndroidInjection
import de.westwing.campaignbrowser.R
import de.westwing.campaignbrowser.databinding.ActivityCampaignDetailsBinding
import de.westwing.campaignbrowser.domain.Campaign


class CampaignDetailsActivity : AppCompatActivity() {

    private val campaign: Campaign by lazy {
        intent.extras?.getParcelable<Campaign>(KEY)
            ?: throw IllegalStateException("Missing arguments")
    }
    lateinit var binding: ActivityCampaignDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCampaignDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(campaign) {
            binding.campaignName.text = name
            binding.campaignDescription.text = description
            Glide
                .with(this@CampaignDetailsActivity)
                .load(campaign.imageUrl)
                .centerCrop()
                .placeholder(R.drawable.image_placeholder)
                .into(binding.campaignImage)
        }

    }

    companion object {
        private const val KEY = "KEY"
        fun newInstance(context: Context, campaign: Campaign) =
            Intent(context, CampaignDetailsActivity::class.java).apply {
                putExtra(KEY, campaign)
            }
    }
}