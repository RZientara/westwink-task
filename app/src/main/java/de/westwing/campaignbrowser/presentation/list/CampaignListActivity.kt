package de.westwing.campaignbrowser.presentation.list

import android.app.Activity
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.AndroidInjection
import de.westwing.campaignbrowser.databinding.ActivityCampaignListBinding
import de.westwing.campaignbrowser.di.ViewModelFactory
import de.westwing.campaignbrowser.domain.Campaign
import de.westwing.campaignbrowser.presentation.CampaignListResult
import de.westwing.campaignbrowser.presentation.CampaignViewModel
import de.westwing.campaignbrowser.presentation.details.CampaignDetailsActivity
import javax.inject.Inject


class CampaignListActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<CampaignViewModel>

    private lateinit var viewModel: CampaignViewModel

    lateinit var binding: ActivityCampaignListBinding

    private val adapter = CampaignListAdapter(itemClick = { c -> handleCampaignClick(c) })

    private fun handleCampaignClick(campaign: Campaign) {
        startActivity(CampaignDetailsActivity.newInstance(this, campaign))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = ActivityCampaignListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this, viewModelFactory).get(CampaignViewModel::class.java)
        if (savedInstanceState == null) {
            //init loading data only on first start
            viewModel.loadData()
        }
        binding.campaignsRecycler.layoutManager = getLayoutManager()
        binding.campaignsRecycler.adapter = adapter

        binding.errorButton.setOnClickListener { viewModel.loadData() }
    }

    private fun getLayoutManager() =
        if (resources.configuration.orientation == ORIENTATION_PORTRAIT) {
            LinearLayoutManager(this)
        } else {
            GridLayoutManager(this, 2)
        }

    override fun onStart() {
        super.onStart()
        viewModel.campaignsData.observe(this, { campaigns ->
            when (campaigns) {
                is CampaignListResult.Success -> processViewState(campaigns.data)
                is CampaignListResult.Error -> showError()
                is CampaignListResult.Loading -> showLoading()
            }
        })
    }

    private fun showLoading() {
        updateErrorVisibility(false)
        binding.campaignsRecycler.isGone = true
        binding.loadingIndicator.isGone = false
    }

    internal fun processViewState(campaigns: List<Campaign>) {
        updateErrorVisibility(false)
        binding.campaignsRecycler.isGone = false
        binding.loadingIndicator.isGone = true
        adapter.submitList(campaigns)
    }

    private fun updateErrorVisibility(show: Boolean) {
        binding.errorButton.isVisible = show
        binding.errorMessage.isVisible = show
    }

    private fun showError() {
        updateErrorVisibility(true)
        binding.campaignsRecycler.isGone = true
        binding.loadingIndicator.isGone = true
    }
}