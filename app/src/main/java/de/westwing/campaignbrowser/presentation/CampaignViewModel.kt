package de.westwing.campaignbrowser.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.westwing.campaignbrowser.domain.GetCampaignListUseCase
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class CampaignViewModel @Inject constructor(
    private val getCampaignListUseCase: GetCampaignListUseCase
) : ViewModel() {

    val campaignsData = MutableLiveData<CampaignListResult>()

    fun loadData() {
        campaignsData.postValue(CampaignListResult.Loading)
        getCampaignListUseCase.execute()
            .subscribeOn(Schedulers.io())
            .doOnError { campaignsData.postValue(CampaignListResult.Error) }
            .onErrorComplete()
            .subscribe { list -> campaignsData.postValue(CampaignListResult.Success(list)) }
    }

}