package de.westwing.campaignbrowser.presentation

import de.westwing.campaignbrowser.domain.Campaign

sealed class CampaignListResult {

    data class Success(val data: List<Campaign>) : CampaignListResult()

    object Error : CampaignListResult()

    object Loading : CampaignListResult()
}