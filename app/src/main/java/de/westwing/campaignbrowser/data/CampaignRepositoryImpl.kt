package de.westwing.campaignbrowser.data

import de.westwing.campaignbrowser.domain.Campaign
import de.westwing.campaignbrowser.domain.CampaignRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class CampaignRepositoryImpl @Inject constructor(private val apiInterface: ApiInterface) :
    CampaignRepository {

    override fun getCampaigns(): Single<List<Campaign>> = apiInterface.getCampaigns()
        .map { response ->
            response.metadata.data
                .filter { it.name.isNotEmpty() && it.description.isNotEmpty() }
                .map { Campaign(it.name, it.description, it.image.url) }
        }

}